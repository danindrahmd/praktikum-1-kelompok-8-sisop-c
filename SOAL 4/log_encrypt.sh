#!/bin/bash

hour=$(date "+%H")
date=$(date "+%H:%M %d:%m:%Y")

backup_filename="/home/keysanadea/SISOP/NO4/soal4encrypt/${date}.txt"

lower_huruf=($(echo {a..z}{a..z}))
upper_huruf=($(echo {A..Z}{A..Z}))

cat /var/log/syslog | tr "$lower_huruf:0:26}${upper_huruf:0:26}" "$lower_huruf:${hour}:26}${upper_huruf:${hour}:26}" > "${backup_filename}"