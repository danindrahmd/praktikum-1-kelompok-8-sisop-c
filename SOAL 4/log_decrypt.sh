#!/bin/bash

file_enc=$(ls -t /home/keysanadea/SISOP/NO4/soal4encrypt/ | head -n1)
enc_filepath="/home/keysanadea/SISOP/NO4/soal4encrypt/${file_enc}"
dec_filepath="/home/keysanadea/SISOP/NO4/soal4decrypt/${file_enc}"

current_hour=$(date "+%H")

lower_huruf=($(echo {a..z}{a..z}))
upper_huruf=($(echo {A..Z}{A..Z}))

if [ -f "$enc_filepath" ]; then
  cat "${enc_filepath}" | tr "${lower_huruf:${current_hour}:26}${upper_huruf:${current_hour}:26}" "${lower_huruf:0:26}${upper_huruf:0:26}" > "${dec_filepath}"
fi